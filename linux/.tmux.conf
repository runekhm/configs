# set ctrl + q as a new modifier
unbind C-b
set -g prefix C-q
bind C-q send-prefix

# true color support
set -ag terminal-overrides ",*:XT@:Tc"

#default history of 5k lines
set -g history-limit 20000

set-option -g xterm-keys on

# splitting panes
bind | split-window -h
bind - split-window -v

# faster switching between windows
bind C-p previous-window
bind C-n next-window

# moving between panes
bind h select-pane -L
bind j select-pane -D
bind k select-pane -U
bind l select-pane -R

# Pane resizing
bind -r H resize-pane -L 5
bind -r J resize-pane -D 5
bind -r K resize-pane -U 5
bind -r L resize-pane -R 5

# Set the default terminal mode to screen-256color mode
#set -g default-terminal "screen-256color"
set -g default-terminal "xterm-256color"
set-window-option -g xterm-keys on

# enable activity alerts
setw -g monitor-activity on
set -g visual-activity on

# set colors for the active window
# pre 2.9
#setw -g window-status-current-fg white
#setw -g window-status-current-bg red
#setw -g window-status-current-attr bright

#post 2.9
setw -g window-status-current-style bg=red,fg=white,bright

# key-binding to reload local tmux config
bind-key R source-file ~/.tmux.conf \; display-message "~/.tmux.conf is reloaded"

# pane movement
bind-key S command-prompt -p "swap with pane:" "swap-window -t '%%'"
#bind-key S command-prompt -p "swap with pane:" "swap-pane -t '%%'"
#bind-key j command-prompt -p "join pane from:"  "join-pane -s '%%'"
#bind-key s command-prompt -p "send pane to:"  "join-pane -t '%%'"

# select pane by number, needs some work
#bind ^A select-pane -t :.+
bind-key P command-prompt -p "Jump to pane:" "select-pane -t '%%'"

#vi mode
setw -g mode-keys vi

# remove escape time since i dont have anything waiting for escape anyways
set -s escape-time 0

#copy and paste
# old syntax
#bind-key -t vi-copy v begin-selection
#bind-key -t vi-copy y copy-selection

# New syntax
bind-key -T copy-mode-vi v send-keys -X begin-selection
bind-key -T copy-mode-vi y send-keys -X copy-selection

unbind ]
bind-key ] paste-buffer \; delete-buffer
#unbind -t vi-copy Enter
#bind-key -t vi-copy Enter copy-pipe "reattach-to-user-namespace pbcopy"

# prefix hightlight
set -g status-right '#{prefix_highlight} | %a %Y-%m-%d %H:%M:%S'

# current window color
# pre 2.9
#set-window-option -g window-status-current-bg red
# post 2.9
setw -g window-status-current-style bg=red

# current pane border color
# pre 2.9
#set-option -g pane-active-border-fg blue
# post 2.9
set-option -g pane-active-border-style fg=blue

#set -g window-style 'fg=colour247,bg=colour236'
#set -g window-active-style 'fg=colour255,bg=black'

# List of plugins
set -g @plugin 'tmux-plugins/tpm'
set -g @plugin 'tmux-plugins/tmux-prefix-highlight'
#set -g @plugin 'tmux-plugins/tmux-sensible'
# Other examples:
# set -g @plugin 'github_username/plugin_name'
# set -g @plugin 'git@github.com/user/plugin'
# set -g @plugin 'git@bitbucket.com/user/plugin'
# Initialize TMUX plugin manager (keep this line at the very bottom of tmux.conf)
run '~/.tmux/plugins/tpm/tpm'
