" autoreload .vimrc
autocmd! bufwritepost .vimrc source %
" enable mouse
" set mouse=a
set bs=2
" Properly disable mouse
set mouse=
set ttymouse=

" yaml
filetype plugin indent on
autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab
set is hlsearch ai ic scs
nnoremap <esc><esc> :nohls<cr>

" Enable folding
set foldmethod=indent
set foldlevel=99

" Space to fold
"nnoremap <space> za

" sorting
vnoremap <Leader>s :sort<CR>
vnoremap <Leader>c "+y

" alternative escape button
inoremap jj <ESC>

" tab as spaces with 4 spaces as default tab
set tabstop=4
set shiftwidth=4
set expandtab

"syntaxhighlighting
"set syntax=on
syntax enable

" ignore case
set ignorecase
set smartcase

" line numbers
set number

"show matching
set showmatch
set incsearch
set hlsearch
" autoindenting
set ai

" toggle paste mode
set pastetoggle=<F2>

if $TMUX == ''
    set clipboard=unnamedplus
    "set clipboard+=unnamed
endif
"set clipboard=unnamedplus,autoselect,exclude:cons\\\\|linux

"set cursorline
set ruler

" move the swap- and backupfile
"set backupdir=~/.vim/backup//
set directory=~/.vim/swp//

" theming
" Set highlighted line bold and lighter text
set cursorline
hi CursorLine term=bold cterm=bold guibg=Grey40
"

" indent move without loosing visual focus
vnoremap < <gv
vnoremap > >gv

" Alt + arrow keys to change tab, og ctrl + arrow keys to switch position
"nnoremap <C-Left> :tabprevious<CR>
"nnoremap <C-Right> :tabnext<CR>
"nnoremap <C-v><C-Left> :tabprevious<CR>
"nnoremap <C-v><C-Right> :tabnext<CR>
nnoremap <silent> <A-Left> :execute 'silent! tabmove ' . (tabpagenr()-2)<CR>
nnoremap <silent> <A-Right> :execute 'silent! tabmove ' . tabpagenr()<CR>
nnoremap <C-h> :tabprevious<CR>
nnoremap <C-l> :tabnext<CR>

if &term =~ '^screen'
    " tmux will send xterm-style keys when its xterm-keys option is on
    execute "set <xUp>=\e[1;*A"
    execute "set <xDown>=\e[1;*B"
    execute "set <xRight>=\e[1;*C"
    execute "set <xLeft>=\e[1;*D"
endif

" F8 to toggle tab list
let notabs = 0
nnoremap <silent> <F8> :let notabs=!notabs<Bar>:if notabs<Bar>:tabo<Bar>:else<Bar>:tab ball<Bar>:tabn<Bar>:endif<CR>

" show trailing whitespace
autocmd ColorScheme * highlight ExtraWhitespace ctermbg=red guibg=red
"
" F5 to run code
au BufEnter * if match( getline(1) , '^\#!') == 0 |
\ execute("let b:interpreter = getline(1)[2:]") |
\endif

"autocmd FileType php noremap <F5> :w!<CR>:!/usr/bin/php %<CR>
fun! CallInterpreter()
    if exists("b:interpreter")
        exec ("!".b:interpreter." %")
    elseif &ft=='php'
        exec(":!/usr/bin/php %")
    endif
endfun

map <F5> :call CallInterpreter()<CR>

" -----------------------------------------------------
"  Plugins
" -----------------------------------------------------
call plug#begin('~/.vim/plugged')
Plug 'https://github.com/junegunn/fzf.vim'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'vim-syntastic/syntastic'
"Plug 'ycm-core/YouCompleteMe'
Plug 'ycm-core/YouCompleteMe', { 'commit':'d98f896' }
call plug#end()


" ----------------------
"  fzf vim
" ----------------------
" Enable fzf
"set rtp+=~/.fzf
set rtp+=~/.nix-profile/share/fzf
" disable statusline overwriting
"let g:fzf_nvim_statusline = 0
"
"" keybindings
nnoremap <C-b> :Buffers<CR>
nnoremap <C-p> :FZF<CR>
nnoremap <C-g>g :Ag<CR>
nnoremap <C-g>c :Commands<CR>
nnoremap <C-f>l :BLines<CR>
"" This is the default extra key bindings
let g:fzf_action = {
  \ 'enter': 'tab split',
  \ 'ctrl-t': 'e',
  \ 'ctrl-x': 'split',
  \ 'ctrl-v': 'vsplit' }

"
"let g:fzf_preview_window = ['top:20%:hidden', 'ctrl-/']
"
"" In Neovim, you can set up fzf window using a Vim command
"let g:fzf_layout = { 'window': 'enew' }
"let g:fzf_layout = { 'window': '-tabnew' }
"let g:fzf_layout = { 'window': '10split enew' }
"
"" Enable per-command history.
":" CTRL-N and CTRL-P will be automatically bound to next-history and
"" previous-history instead of down and up. If you don't like the change,
"" explicitly bind the keys to down and up in your $FZF_DEFAULT_OPTS.
"let g:fzf_history_dir = '~/.local/share/fzf-history'
